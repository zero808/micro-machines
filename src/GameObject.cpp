#include "GameObject.h"

void GameObject::update(GLdouble delta) {
}

void GameObject::defineMaterial(GLfloat a_r, GLfloat a_g, GLfloat a_b, GLfloat a_w,
	GLfloat d_r, GLfloat d_g, GLfloat d_b, GLfloat d_w,
	GLfloat s_r, GLfloat s_g, GLfloat s_b, GLfloat s_w,
	GLfloat Si) {
	GLfloat amb[4] = { a_r, a_g, a_b, a_w };
	GLfloat dif[4] = { d_r, d_g, d_b, d_w };
	GLfloat spe[4] = { s_r, s_g, s_b, s_w };
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, dif);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spe);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &Si);

}