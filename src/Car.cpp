#include "Car.h"
#include "GameManager.h"
#include <iostream>

Car::Car() : direction(false){
    position.set(CAR_INITIAL_POSITION_X, CAR_INITIAL_POSITION_Y, CAR_INITIAL_POSITION_Z);
	setCollisionRadius(0.31f);
}

void Car::drawwheel(GLdouble x, GLdouble y, GLdouble z, bool wired) {
	
	glPushMatrix();
	defineMaterial(0.02, 0.02, 0.02, 1.00,	//Ambient
		0.01, 0.01, 0.01, 1.00,	//Diffuse
		0.4, 0.4, 0.4, 1.00,	//Specular
		100);					//SHININESS
	glColor3f(0.0f, 0.0f, 0.0f);
	glTranslatef(x, y, z);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	if (wired)
		glutWireTorus(0.025f, 0.025f, 20, 20);
	else
		glutSolidTorus(0.025f, 0.025f, 20, 20);
	glPopMatrix();
}

void Car::drawCube(GLdouble x, GLdouble y, GLdouble z, bool wired, GLdouble size) {
	glPushMatrix();
	defineMaterial(0.6, 0.0, 0, 1.00,	//Ambient
		0.6, 0.0, 0, 1.00,	//Diffuse
		0.6, 0.0, 0, 1.00,	//Specular
		77);					//SHININESS
	glColor3f(1.0f, 0.0f, 0.0f);
	glTranslatef(x, y, z); 
	if (wired)
		glutWireCube(size);
	else
		glutSolidCube(size);
	glPopMatrix();
}

void Car::draw() {
    bool wired = gm.getWired();
    GLdouble x = position.getX();
    GLdouble y = position.getY();
    GLdouble z = position.getZ();

    // Carro -> 3 Cubos + 4 Torus

	glPushMatrix();
		glTranslatef(x, y, z);
		glRotatef(rotation, 0.0f, 0.0f, 1.0f);
		drawwheel(-0.1f, 0.10f, 0.175f, wired);
		drawwheel(-0.1f, -0.10f, 0.175f, wired);
		drawwheel(0.1f, 0.10f, 0.175f, wired);
		drawwheel(0.1f, -0.10f, 0.175f, wired);
		drawCube(-0.1f, 0.0f, 0.3f, wired, 0.2f);
		drawCube(0.1f, 0.0f, 0.3f, wired, 0.2f);
	glPushMatrix();					//cabe�a
		glColor3f(1.0f, 1.0f, 0.0f);
		defineMaterial(0.05, 0.05, 0.0, 1.0,	//Ambient
		0.5, 0.5, 0.4, 1.0,	//Diffuse
		0.7, 0.7, 0.04, 1.0,	//Specular
		100);					//SHININESS
		glTranslatef(-0.1f, 0.0f, 0.45f);
		if (wired)
			glutWireCube(0.1f);
		else
			glutSolidCube(0.1f);
	glPopMatrix();
	glPopMatrix();
	glFlush();
}

bool Car::getDirection() {
	return direction;
}

void Car::flipDirection() {
    direction = !direction;
}

void Car::update(GLdouble delta_t){
	
	setPosition(position.getX() + speed.getX(),position.getY() + speed.getY(), 0.0f);

	GLdouble unitary = sqrtf(pow(speed.getX(), 2) + pow(speed.getY(), 2));
	GLdouble coordx = cos((rotation * PI)/180);
	GLdouble coordy = sin((rotation * PI)/180);

	if (position.getX() > 4.5f || position.getY() > 4.5f || position.getX() < -4.5f || position.getY() < -4.5f) {
		setPosition(0.0f, 0.0f, 0.0f);
		speed.set(0.0f, 0.0f, 0.0f);
	}
	else {
		if (unitary <= 0.0001f)
			unitary = 0;
		else
			if (getDirection())
				speed.set((-unitary + 0.0001f) * coordx, (-unitary + 0.0001f) * coordy, 0.0f);
			else
				speed.set((unitary - 0.0001f) * coordx, (unitary - 0.0001f) * coordy, 0.0f);
	}
}