#include <cstdlib>
#include <ctime>
#include "GameManager.h"
#include "Table.h"
#include "Road.h"
#include "Butter.h"
#include "Orange.h"
#include "Car.h"
#include "Cheerio.h"
#include <cmath>
#include <iostream>

GameManager::GameManager() : wired (false) {
    init();
}

void GameManager::init() {
    size_t count = CHEERIO_AMOUNT + BUTTER_AMOUNT + ORANGE_AMOUNT + 1;
	srand(time(NULL));

	//Inicializacao da luz
	light = new LightSource(GL_LIGHT0 + 0, false);
	light->setDirection(0, -1, 0);
	light->setAmbient(MAIN_LIGHT_AMBIENTR, MAIN_LIGHT_AMBIENTG, MAIN_LIGHT_AMBIENTB, MAIN_LIGHT_AMBIENTA);
	light->setDiffuse(MAIN_LIGHT_DIFFUSER, MAIN_LIGHT_DIFFUSEG, MAIN_LIGHT_DIFFUSEB, MAIN_LIGHT_DIFFUSEA);
	light->setSpecular( MAIN_LIGHT_SPECULARR,  MAIN_LIGHT_SPECULARG,  MAIN_LIGHT_SPECULARB,  MAIN_LIGHT_SPECULARA);

	//luz ambiente
	GLfloat ambientColor[] = { 0.2f, 0.2f, 0.2f, 1.0f }; //Color(0.2, 0.2, 0.2)
	/*if (glIsEnabled(GL_LIGHTING))
		puts("yes");*/
	glEnable(GL_LIGHTING);
	/*if (glIsEnabled(GL_LIGHTING))
		puts("yes");*/
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);

	//pointlights
	for (size_t i = 0; i < CANDLES_AMOUNT; ++i) {
		candles[i].setNum(GL_LIGHT1 + i);
		candles[i].setPosition(4.0f * cos(360 / CANDLES_AMOUNT * i * PI / 180), 4.0f * sin(360 / CANDLES_AMOUNT * i * PI / 180), 1.0f);
		candles[i].setAmbient(0.6, 0.6, 0.6, 1);
		candles[i].setDiffuse(0.6, 0.6, 0.6, 1);
		candles[i].setSpecular(0.6, 0.6, 0.6, 1);
	}

	//objects
    for (int i = 0; i < CHEERIO_AMOUNT; i++) {
        game_objects.push_back(new Cheerio(2.0f * cos(30.0f * i * PI / 180), 2.0f * sin(30.0f * i * PI / 180), 0.0f));
        game_objects.push_back(new Cheerio(4.0f * cos(30.0f * i * PI / 180), 4.0f * sin(30.0f * i * PI / 180), 0.0f));
    }
    for (int i = 0; i < BUTTER_AMOUNT; ++i) {
        game_objects.push_back(new Butter(rand() % 100 / 10 - 5, rand() % 100 / 10 - 5, 0.25f));
    }
    for (int i = 0; i < ORANGE_AMOUNT; ++i) {
        game_objects.push_back(new Orange(rand() % 100 / 10 - 5, rand() % 100 / 10 - 5, 0.25f));
    }

    car = new Car();
	road = new Road();
    table = new Table();
    game_objects.push_back(car);

	//cameras
    ocam = new OrthogonalCamera();
    fcam = new PerspectiveCamera(FIXED_CAMERA_FOVY, FIXED_CAMERA_ASPECT, FIXED_CAMERA_NEAR, FIXED_CAMERA_FAR);
    fcam->setCenter(FIXED_CAMERA_CENTERX, FIXED_CAMERA_CENTERY, FIXED_CAMERA_CENTERZ);
    fcam->setAt(FIXED_CAMERA_EYEX, FIXED_CAMERA_EYEY, FIXED_CAMERA_EYEZ);
    fcam->setUp(FIXED_CAMERA_UPX, FIXED_CAMERA_UPY, FIXED_CAMERA_UPZ);

    carcam = new PerspectiveCamera(CAR_CAMERA_FOVY, CAR_CAMERA_ASPECT, CAR_CAMERA_NEAR, CAR_CAMERA_FAR);
    carcam->setCenter(CAR_CAMERA_CENTERX, CAR_CAMERA_CENTERY, CAR_CAMERA_CENTERZ);
    carcam->setAt(CAR_CAMERA_EYEX, CAR_CAMERA_EYEY, CAR_CAMERA_EYEZ);
    carcam->setUp(CAR_CAMERA_UPX, CAR_CAMERA_UPY, CAR_CAMERA_UPZ);
	
}

GameManager::~GameManager() {
    for(std::vector<DynamicObject*>::iterator it = game_objects.begin(); it != game_objects.end(); ++it) {
        delete *it;
    }
    delete road;
    delete table;
    delete ocam;
    delete fcam;
    delete carcam;
	delete light;
}

PerspectiveCamera* GameManager::getCarcam() {
    return carcam;
}

void GameManager::display() {
    if(ocam->isActive()) {
		ocam->computeProjectionMatrix();
		ocam->computeVisualizationMatrix();
	}
	if(fcam->isActive()) {
		fcam->computeProjectionMatrix();
		fcam->computeVisualizationMatrix();
	}
	if(carcam->isActive()) {
		carcam->computeProjectionMatrix(); 
		carcam->computeVisualizationMatrix();
	}

    table->draw();
    road->draw();
    for(std::vector<DynamicObject*>::iterator it = game_objects.begin(); it != game_objects.end(); ++it) {
        (*it)->draw();
    }
	if(light->getState())
		light->draw();
	for (auto it = candles.begin(); it != candles.end(); ++it) {
		/*for(size_t i = 0; i < CANDLES_AMOUNT; ++i) {*/
			/*if(it->getState())*/
				it->draw();
			//if(candles[i].getState())
				/*candles[i].draw();*/
	}
    glutSwapBuffers();
}

void GameManager::reshape(GLsizei w, GLsizei h) {
    GLfloat ratio = (XMAX - XMIN) / (YMAX - YMIN);
    GLfloat aspect = (GLfloat) w / h;

    //update the aspect of the two perspective cameras
    fcam->setAspect(aspect);
    carcam->setAspect(aspect);

    //Viewport
    if(aspect > ratio)
        glViewport((w - h * ratio) / 2, 0, h * ratio, h);
    else
        glViewport(0, (h - w / ratio) / 2, w, w / ratio);
}

//void flipLightning() { static bool b = false; b ? glDisable(GL_LIGHTING) : glEnable(GL_LIGHTING); b = !b; }
void flipLightning() {
	static bool b = true;
	if (b)
		glDisable(GL_LIGHTING);
	else {
		glEnable(GL_LIGHTING);
	}
	b = !b;
}

void GameManager::flipCandles() {
	for (auto it = candles.begin(); it != candles.end(); ++it) {
		it->getState() ? glDisable(it->getNum()) : glEnable(it->getNum());
		it->flipState();
	}
}

void GameManager::keyPressed(unsigned char key, int x, int y) {
    switch(key) {
        case 'Q':
        case 'q': { exit(0);}
        case 'A':
        case 'a': { flipWired(); break;} 
		case 'N':
		case 'n': {
			if (light->getState()) {
				glDisable(GL_LIGHT0);
			}
			else {
				glEnable(GL_LIGHT0);
			}
			light->flipState();
			break; 
		}
		case 'C':
		case 'c': { flipCandles(); break; }
		case 'L':
		case 'l': {
			flipLightning(); break;
		}

        case '1': {
                      if(!ocam->isActive()) {
						ocam->flipActive();
						ocam->computeProjectionMatrix();
						ocam->computeVisualizationMatrix();
						if(fcam->isActive()) fcam->flipActive();
						if(carcam->isActive()) carcam->flipActive();
					  }
                      break;
                  }
        case '2': {
                      if(!fcam->isActive()) {
						fcam->flipActive();
						if(ocam->isActive()) ocam->flipActive();
						if(carcam->isActive()) carcam->flipActive();
						fcam->computeProjectionMatrix();
						fcam->computeVisualizationMatrix();
					  }
                      break;
                  }
        case '3': {
                      if(!carcam->isActive()) {
						carcam->flipActive();
						if(fcam->isActive()) fcam->flipActive();
						if(ocam->isActive()) ocam->flipActive();
						carcam->computeProjectionMatrix();
						carcam->computeVisualizationMatrix();
					  }
                      break;
                  }
        default: 
                  break;
    }
}

void GameManager::keyReleased(unsigned char key, int x, int y) {
    switch (key) {
        case 'j': {
                      break;
                  }
        case 'l': {
                      break;
                  }
        case 'i': {
                      break;
                  }
        case 'k': {
                      break;
                  }
        default:
                  break;
    }
}

void GameManager::specialKeyPressed(int key, int x, int y) {
	GLdouble rotation;
    switch(key) {
		case GLUT_KEY_RIGHT: {
			car->setRotation(car->getRotation()-15.0f);
                      rotation = car->getRotation();
                      Vector3 s = car->getSpeed();
                      GLdouble unitary = sqrtf(pow(s.getX(), 2) + pow(s.getY(), 2));
                      GLdouble coordx = cos((rotation * PI)/180);
                      GLdouble coordy = sin((rotation * PI)/180);
                      if(car->getDirection())
                          car->setSpeed(-unitary * coordx, -unitary  * coordy, 0.0f);
                      else
                          car->setSpeed(unitary * coordx, unitary  * coordy, 0.0f);
                      break;
                  }
        case GLUT_KEY_LEFT: {
                      car->setRotation(car->getRotation()+15.0f);
                      rotation = car->getRotation();
                      Vector3 s = car->getSpeed();
                      GLdouble unitary = sqrtf(pow(s.getX(), 2) + pow(s.getY(), 2));
                      GLdouble coordx = cos((rotation * PI)/180);
                      GLdouble coordy = sin((rotation * PI)/180);
                      if(car->getDirection())
                          car->setSpeed(-unitary * coordx, -unitary  * coordy, 0.0f);
                      else
                          car->setSpeed(unitary * coordx, unitary  * coordy, 0.0f);
                      break;
                  }
		case GLUT_KEY_UP: {
                      rotation = car->getRotation();
                      Vector3 s = car->getSpeed();
                      GLdouble unitary = sqrtf(pow(s.getX(), 2) + pow(s.getY(), 2));
                      GLdouble coordx = cos((rotation * PI)/180);
                      GLdouble coordy = sin((rotation * PI)/180);
                      if(unitary < 0.01f){
                          if(car->getDirection() == true)
                              car->flipDirection();
                          car->setSpeed((unitary + 0.01f) * coordx, (unitary + 0.01f) * coordy, 0.0f);
                      }
                      else{
                          if(car->getDirection())
                              car->setSpeed((-unitary + 0.01f) * coordx, (-unitary + 0.01f) * coordy, 0.0f);
                          else
                              car->setSpeed((unitary + 0.01f) * coordx, (unitary + 0.01f) * coordy, 0.0f);
                      }
                      break;
                  }
        case GLUT_KEY_DOWN: {
                      rotation = car->getRotation();
                      Vector3 s = car->getSpeed();
                      GLdouble unitary = sqrtf(pow(s.getX(), 2) + pow(s.getY(), 2));
                      GLdouble coordx = cos((rotation * PI)/180);
                      GLdouble coordy = sin((rotation * PI)/180);

                      if (unitary <= 0.01f) {
                          if (car->getDirection() == false){
                              car->flipDirection();
                          }
                          car->setSpeed((-unitary - 0.01f) * coordx, (-unitary - 0.01f) * coordy, 0.0f);
                      }
                      else{
                          if(car->getDirection())
                              car->setSpeed((-unitary - 0.01f) * coordx, (-unitary - 0.01f) * coordy, 0.0f);
                          else
                              car->setSpeed((unitary - 0.01f) * coordx, (unitary - 0.01f) * coordy, 0.0f);
                      }
                      break;
                  }
		default:
			break;
	}
}

void GameManager::collision() {
    Vector3 c = car->getPosition();
    GLdouble p = car->getCollisionRadius();
    Vector3 v = car->getSpeed();
	GLdouble r = car->getRotation();
	Vector3 o;
	GLdouble k;
    for (std::vector<DynamicObject*>::iterator it = game_objects.begin(); it != game_objects.end(); ++it) {
        o = (*it)->getPosition();
        k = (*it)->getCollisionRadius();
        if (k != 0.31f){ // car
            if (k == 0.26f) { // orange
				if(sqrt( ( o.getX()- c.getX()) * ( o.getX()- c.getX() )  + ( o.getY()- c.getY() ) * ( o.getY()- c.getY() ) ) < ( p + k ) ) {
                        car->setSpeed(0.0f, 0.0f, 0.0f);
                        car->setPosition(0.0f, 0.0f, 0.0f);
                    }
				
            }
            else {
               if(sqrt( ( o.getX()- c.getX()) * ( o.getX()- c.getX() )  + ( o.getY()- c.getY() ) * ( o.getY()- c.getY() ) ) < ( p + k ) ) {
						if(car->getDirection())
							(*it)->setRotation(r+180);
						else
							(*it)->setRotation(r);
						(*it)->setSpeed(v);
                        car->setSpeed(0.0f, 0.0f, 0.0f);
                    }
            }
        }
    }
}

void GameManager::onTimer() {
	gm.update();
	glutPostRedisplay();    // Post a paint request to activate display()
    glutTimerFunc(10, timer, 0); // subsequent timer call at milliseconds
}
void GameManager::idle() {
}
void GameManager::update() {
    for(std::vector<DynamicObject*>::iterator it = game_objects.begin(); it != game_objects.end(); ++it) {
        (*it)->update(0); //No getspeed on static objects
    }
	collision();
	if (carcam->isActive()) {
		GLdouble rotation = car->getRotation();
		Vector3 v = car->getPosition();
		GLdouble unitary = sqrtf(pow(v.getX(), 2) + pow(v.getY(), 2));
		GLdouble coordx = cos((rotation * PI) / 180);
		GLdouble coordy = sin((rotation * PI) / 180);
		carcam->setAt(v.getX() - (2 * coordx), v.getY() - (2 *coordy), 1.5f);
		carcam->setCenter(v.getX(), v.getY(), 0.0f);
		carcam->computeProjectionMatrix();
	}
}

bool GameManager::getWired() {
    return wired;
}

void GameManager::flipWired() {
    wired = !wired;
}

void display() {
    gm.display();
}

void reshape(int w, int h) {
    gm.reshape(w, h);
}

void keyPressed(unsigned char key, int x, int y) {
    gm.keyPressed(key, x, y);
}

void specialKeyPressed(int key, int x, int y) {
	gm.specialKeyPressed(key, x, y);
}

void keyUp(unsigned char key, int x, int y) {
    gm.keyReleased(key, x, y);
}

void timer(int value) {
    gm.onTimer();
}

void update(int i) {
    gm.update();
    glutTimerFunc(10, update, 0);
}
