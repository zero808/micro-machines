#include "PerspectiveCamera.h"
#include <iostream>

GLdouble PerspectiveCamera::getFovy() const
{
	return fovy;
}

GLdouble PerspectiveCamera::getAspect() const
{
	return aspect;
}

void PerspectiveCamera::setFovy(GLdouble f)
{
	fovy = f;
}

void PerspectiveCamera::setAspect(GLdouble a)
{
	aspect = a;
}

void PerspectiveCamera::update(Car *c)
{
	/*Vector3 v = c->getPosition();
	this->position.set(v.getX() - 1, v.getY() - 1, CAR_CAMERA_CENTERZ);*/
	//actualizar o AT
	// o UP mant�m-se o mesmo?
}

void PerspectiveCamera::computeProjectionMatrix()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fovy, aspect,near,far);
	gluLookAt(at.getX(),at.getY(),at.getZ(),center.getX(),center.getY(),center.getZ(),up.getX(),up.getY(),up.getZ());
	glMatrixMode(GL_MODELVIEW);
}

void PerspectiveCamera::computeVisualizationMatrix(){
}
