#include <iostream>
#include "GameManager.h"
#include "Butter.h"

Butter::Butter(GLdouble x, GLdouble y, GLdouble z) : Obstacle(x, y, z) {
	setCollisionRadius(0.3f);
	setAlive(0);
	setRotation(0.0f);
}

void Butter::draw() {
    glPushMatrix();
    glColor3f(1.0f, 1.0f, 0.0f);
	defineMaterial(0.05, 0.05, 0.0, 1.0,	//Ambient
		0.5, 0.5, 0.4, 1.0,	//Diffuse
		0.7, 0.7, 0.04, 1.0,	//Specular
		100);					//SHININESS
    glPushMatrix();

    glTranslatef(position.getX(), position.getY(), position.getZ());
    if(gm.getWired())
        glutWireCube(0.5f);
    else
        glutSolidCube(0.5f);
    glPopMatrix();
    glPopMatrix();
    glFlush();
}

void Butter::update(GLdouble delta_t) {

		setPosition(position.getX() + speed.getX(), position.getY() + speed.getY(), 0.25f);

		GLdouble unitary = sqrtf(pow(speed.getX(), 2) + pow(speed.getY(), 2));
		GLdouble coordx = cos((rotation * PI) / 180);
		GLdouble coordy = sin((rotation * PI) / 180);
		//orange out of bounds
		if (unitary <= 0.0001f)
			unitary = 0;
		else
			if(position.getX() > 4.7f || position.getY() > 4.7f || position.getX() < -4.7f || position.getY() < -4.7f)
				speed.set(0.0f,0.0f,0.0f);
			else
				speed.set((unitary - 0.0001f) * coordx, (unitary - 0.0001f) * coordy, 0.0f);
}
