#include "LightSource.h"

std::array<GLfloat, 4> LightSource::getAmbient() const {
	return ambient;
}

void LightSource::setAmbient(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
	ambient[0] = r;
	ambient[1] = g;
	ambient[2] = b;
	ambient[3] = a;
}

std::array<GLfloat, 4> LightSource::getDiffuse() const {
	return diffuse;
}
void LightSource::setDiffuse(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
	diffuse[0] = r;
	diffuse[1] = g;
	diffuse[2] = b;
	diffuse[3] = a;
}
std::array<GLfloat, 4> LightSource::getSpecular() const {
	return specular;
}
void LightSource::setSpecular(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
	specular[0] = r;
	specular[1] = g;
	specular[2] = b;
	specular[3] = a;
}
GLdouble LightSource::getCutOff() const {
	return cut_off;
}

void LightSource::setCutOff(GLdouble co) {
	cut_off = co;
}

GLdouble LightSource::getExponent() const {
	return exponent;
}

void LightSource::setExponent(GLdouble e) {
	exponent = e;
}

GLboolean LightSource::getState() const {
	return state;
}

void LightSource::flipState() {
	state = !state;
}

void LightSource::setNum(GLenum n) {
	num = n;
}

GLenum LightSource::getNum() const {
	return num;
}

Vector3 LightSource::getDirection() const {
	return direction;
}

void LightSource::setDirection(GLdouble x, GLdouble y, GLdouble z) {
	direction.set(x, y, z);
	GLfloat aux[3] = { x, y, z };
	glLightfv(num, GL_SPOT_DIRECTION, aux);
}

Vector3 LightSource::getPosition() const {
	return position;
}

void LightSource::setPosition(GLdouble x, GLdouble y, GLdouble z) {
	position.set(x, y, z);
	GLfloat aux[4] = { x, y, z, 1.0f };
	glLightfv(num, GL_POSITION, aux);
}

void LightSource::draw() {
	GLfloat dir[4] = { 0, 0, -1, 0 };

	glPushMatrix();
	glLightfv(num, GL_SPOT_DIRECTION, dir);
	glLightfv(num, GL_AMBIENT, ambient.data());
	glLightfv(num, GL_DIFFUSE, diffuse.data());
	glLightfv(num, GL_SPECULAR, specular.data());
	glLightf(num, GL_QUADRATIC_ATTENUATION, .1);
	glPopMatrix();
}