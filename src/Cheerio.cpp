#include <cstdlib>
#include <iostream>
#include "GameManager.h"
#include "Cheerio.h"
#include "Car.h"

Cheerio::Cheerio(GLdouble x, GLdouble y, GLdouble z) : Obstacle(x, y, z) {
	setCollisionRadius(0.1f);
	setAlive(0);
	setRotation(0.0f);
}

void Cheerio::draw() {
	glColor3f(1.0f, 0.7f, 0.0f);
	defineMaterial(0.67, 0.56, 0.48, 1.00,	//Ambient
		0.37, 0.35, 0.33, 1.00,	//Diffuse
		0.0, 0.0, 0.0, 1.00,	//Specular
		77);					//SHININESS
	glPushMatrix();
	glTranslatef(position.getX(), position.getY(), position.getZ());
	glutSolidTorus(0.05f, 0.1f, 20, 20);
	glPopMatrix();
	glFlush();
}

void Cheerio::update(GLdouble delta_t) {

		setPosition(position.getX() + speed.getX(), position.getY() + speed.getY(), 0.25f);

		GLdouble unitary = sqrtf(pow(speed.getX(), 2) + pow(speed.getY(), 2));
		GLdouble coordx = cos((rotation * PI) / 180);
		GLdouble coordy = sin((rotation * PI) / 180);
		//orange out of bounds
		if (unitary <= 0.0001f)
			unitary = 0;
		else
			if(position.getX() > 4.9f || position.getY() > 4.9f || position.getX() < -4.9f || position.getY() < -4.9f)
				speed.set(0.0f,0.0f,0.0f);
			else
				speed.set((unitary - 0.0001f) * coordx, (unitary - 0.0001f) * coordy, 0.0f);
	}