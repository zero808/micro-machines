#include "GameManager.h"
#include "GL/glut.h"

GameManager gm;
int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(1000, 1000);
    glutInitWindowPosition(-1, -1);
    glutCreateWindow("MicroMachines");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
	glutKeyboardFunc(keyPressed);
	glutSpecialFunc(specialKeyPressed);
	glutTimerFunc(1, timer, 0);
	glEnable(GL_LIGHTING);
	//glutFullScreen();
    glutMainLoop();
    return 0;
}