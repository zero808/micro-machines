#include "DynamicObject.h"
#include <iostream>

DynamicObject::DynamicObject() : rotation(0.0f) {}

void DynamicObject::update(GLdouble delta_t) {
}

void DynamicObject::setSpeed(const Vector3 &v) {
	speed = v;
}

void DynamicObject::setSpeed(GLdouble x, GLdouble y, GLdouble z) {
	speed.set(x ,y ,z);
}

Vector3 DynamicObject::getSpeed() const {
	return speed;
}

GLdouble DynamicObject::getRotation() {
    return rotation;
}

void DynamicObject::setRotation(GLdouble angle) {
	rotation = angle; 
	if(rotation < 0) {
		rotation = 360 + rotation;
		return;
	}
	if(rotation > 360)
		rotation = rotation - 360;
}

GLdouble DynamicObject::getCollisionRadius() {
	return collisionRadius;
}

void DynamicObject::setCollisionRadius(GLdouble radius) {
	collisionRadius = radius;
}

GLdouble DynamicObject::getAlive() {
	return alive;
}

void DynamicObject::setAlive(GLdouble tempo) {
	alive = tempo;
}