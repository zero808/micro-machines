#include "OrthogonalCamera.h"

GLdouble OrthogonalCamera::getLeft() const
{
	return left;
}

GLdouble OrthogonalCamera::getRight() const
{
	return right;
}

GLdouble OrthogonalCamera::getBottom() const
{
	return bottom;
}

GLdouble OrthogonalCamera::getTop() const
{
	return top;
}

void OrthogonalCamera::setLeft(GLdouble l)
{
	left = l;
}

void OrthogonalCamera::setRight(GLdouble r)
{
	right = r;
}

void OrthogonalCamera::setBottom(GLdouble b)
{
	bottom = b;
}

void OrthogonalCamera::setTop(GLdouble t)
{
	top = t;
}

void OrthogonalCamera::update(Car*)
{
	//empty because the camera is in a fixed position and stays there (for now)
}

void OrthogonalCamera::computeProjectionMatrix()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(left, right, bottom, top, near, far);
	glMatrixMode(GL_MODELVIEW);
}

void OrthogonalCamera::computeVisualizationMatrix()
{
	//empty
}
