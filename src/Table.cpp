#include <iostream>
#include "Table.h"

void Table::draw() {

    GLfloat i = 0.0f, j = 0.0f;
    GLint k = 0;

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        glLoadIdentity();
    glColor3f(0.5f, 0.5f, 0.5f); //CUBO da Mesa
	defineMaterial(0.5, 0.5, 0.5, 1.00,	//Ambient
		0.5, 0.5, 0.5, 1.00,	//Diffuse
		0.5, 0.5, 0.5, 1.00,	//Specular
		128);					//SHININESS	
	glPushMatrix();
	glTranslatef(0.0f,0.0f,-5.0f);
    /*glutSolidCube(10.0f);*/

	glBegin(GL_QUADS);                // Begin drawing the color cube with 6 quads

	// Back face (z = -1.0f)
	  glNormal3f(0, 0, -1);
      glVertex3f( 5.0f, -5.0f, -5.0f);
      glVertex3f(-5.0f, -5.0f, -5.0f);
      glVertex3f(-5.0f,  5.0f, -5.0f);
      glVertex3f( 5.0f,  5.0f, -5.0f);

      // Top face (y = 1.0f)
      // Define vertices in counter-clockwise (CCW) order with normal pointing out
	  glNormal3f(0, 1, 0);
      glVertex3f( 5.0f, 5.0f, -5.0f);
      glVertex3f(-5.0f, 5.0f, -5.0f);
      glVertex3f(-5.0f, 5.0f,  5.0f);
      glVertex3f( 5.0f, 5.0f,  5.0f);
 
      // Bottom face (y = -1.0f)
	  glNormal3f(0, -1, 0);
      glVertex3f( 5.0f, -5.0f,  5.0f);
      glVertex3f(-5.0f, -5.0f,  5.0f);
      glVertex3f(-5.0f, -5.0f, -5.0f);
      glVertex3f( 5.0f, -5.0f, -5.0f);
 
      // Left face (x = -1.0f)
      glNormal3f(-1, 0, 0);
      glVertex3f(-5.0f,  5.0f,  5.0f);
      glVertex3f(-5.0f,  5.0f, -5.0f);
      glVertex3f(-5.0f, -5.0f, -5.0f);
      glVertex3f(-5.0f, -5.0f,  5.0f);
 
      // Right face (x = 1.0f)
      glNormal3f(1, 0, 0);
      glVertex3f(5.0f,  5.0f, -5.0f);
      glVertex3f(5.0f,  5.0f,  5.0f);
      glVertex3f(5.0f, -5.0f,  5.0f);
      glVertex3f(5.0f, -5.0f, -5.0f);

	   // Front face  (z = 1.0f)
	  glNormal3f(0, 0, 1);
      glVertex3f( 5.0f,  5.0f, 5.0f);
      glVertex3f(-5.0f,  5.0f, 5.0f);
      glVertex3f(-5.0f, -5.0f, 5.0f);
      glVertex3f( 5.0f, -5.0f, 5.0f);
   glEnd();  // End of drawing color-cube

    glPopMatrix();
    glColor3f(0.0f, 0.0f, 0.7f); //Preenchemento do topo do mesa, ficando com um aspecto de xadrez
	defineMaterial(0.0, 0.0, 1, 1.00,	//Ambient
		0.28, 0.52, 0.2, 1.00,	//Diffuse
		0.0, 0.0, 0.0, 1.00,	//Specular
		128);					//SHININESS
    glPushMatrix();
        glTranslatef(0.0f, 0.0f, 0.0f);
        for (i = -4.0f; i <= 5; i++ , k++) {
            if (k % 2 == 0)
                j = -4.0f;
            else
                j = -3.0f;
                    for (; j <= 5; j = j + 2) {
                        glPushMatrix();
                            glTranslatef(i, j, 0.0f);
                            glBegin(GL_POLYGON);
							glNormal3f(0.0f,0.0f,1.0f);
                            glVertex3f(-1.0f, -1.0f, 0.0f);
                            glVertex3f(0.0f, -1.0f, 0.0f);
                            glVertex3f(0.0f, 0.0f, 0.0f);
                            glVertex3f(-1.0f, 0.0f, 0.0f);
                            glEnd();
                            glPopMatrix();
                    }
        }
    glPopMatrix();
    glFlush();
}
