#include "Vector3.h"
#include <cmath>

Vector3 Vector3::operator=(const Vector3& other) {
    /* x = other.getX(); */
    x = other.x;
	y = other.y;
	z = other.z;
    return *this;
}

Vector3 Vector3::operator+(const Vector3& other) {
     x += other.x;
     y += other.y;
     z += other.z;
     return *this;
}

Vector3 Vector3::operator*(GLdouble num) {
    x *= num;
    y *= num;
    z *= num;
    return *this;
}

Vector3 Vector3::operator-(const Vector3& other) {
     x -= other.x;
     y -= other.y;
     z -= other.z;
     return *this;
}
bool Vector3::operator==(const Vector3& other) {
     return (x == other.x) && (y == other.y) && (z == other.z);
}

//Vector3 Vector3::normalize(){
//	Vector3 vector;
//	GLdouble length = sqrt(x * x + y * y + z * z);
// 
//    if(length != 0){
//        vector.setX(x/length);
//		vector.setY(y/length);
//		vector.setZ(z/length);
//    }
// 
//    return vector;
//}

void Vector3::normalize(){
	/*Vector3 vector;*/
	GLdouble length = sqrt(x * x + y * y + z * z);
 
    if(length != 0){
        x = (x/length);
		y = (y/length);
		z = (z/length);
    }
 
    //return vector;
}
