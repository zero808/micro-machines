#include <iostream>
#include "Road.h"
#include "GameManager.h"
#include "solids.h"


void Road::draw() {

    glPushMatrix(); //Estrada
    glColor3f(0.0f, 0.5f, 0.0f);
	defineMaterial(0.0, 0.0, 0.0, 1.0,	//Ambient
		0.0, 0.0, 0.0, 1.0,	//Diffuse
		1, 1, 1, 1.0,	//Specular
		128);					//SHININESS
    glTranslatef(0.0f, 0.0f, 0.0f);
    //glutSolidTorus(1.0f, 3.0f, 20, 20);
	//DrawTorus(1.0f, 3.0f, 20, 20);
	for(GLint i = 0 ; i < 12 ; i++){
	glBegin(GL_POLYGON);
	glNormal3f(0.0f,0.0f,1.0f);
	glVertex3f(2.0f * cos(30.0f * i * PI / 180), 2.0f * sin(30.0f * i * PI / 180), 0.0f);
	glVertex3f(4.0f * cos(30.0f * i * PI / 180), 4.0f * sin(30.0f * i * PI / 180), 0.0f);
	glVertex3f(4.0f * cos(30.0f * (i+1) * PI / 180), 4.0f * sin(30.0f * (i+1) * PI / 180), 0.0f);
	glVertex3f(2.0f * cos(30.0f * (i+1)* PI / 180), 2.0f * sin(30.0f * (i+1) * PI / 180), 0.0f);
	glEnd();
	}
    glPopMatrix();
    glFlush();
}
