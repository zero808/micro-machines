#include "Entity.h"


Entity::Entity() {
    //position = new Vector3();
}

Entity::~Entity() {
    /* for(Vector3* v : position) { */
    /*     delete v; */
    /* } */
    /* position.clear(); */
}

Vector3 Entity::getPosition() const {
    return position;
}

Vector3 Entity::setPosition(GLdouble x, GLdouble y, GLdouble z) {
    //position->set(x, y, z);
	position.set(x, y, z);
	return position;
}

Vector3 Entity::setPosition(const Vector3 &vec) {
    position = vec;
	return position;
}

