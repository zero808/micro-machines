#include "PointLight.h"
//#include <iostream>

PointLight::PointLight() : LightSource() {}

PointLight::PointLight(GLenum _num, GLboolean st) : LightSource(_num, st) {}

PointLight::~PointLight() {}

void PointLight::draw() {
	GLfloat pos[4] = { position.getX(), position.getY(), position.getZ(), 1 };

	if(state) {
		glPushMatrix();
		glLightfv(num, GL_POSITION, pos);
		glLightfv(num, GL_AMBIENT, ambient.data());
		glLightfv(num, GL_DIFFUSE, diffuse.data());
		glLightfv(num, GL_SPECULAR, specular.data());
		/*glLightf(num, GL_SPOT_CUTOFF, 360);*/
		glLightf(num, GL_QUADRATIC_ATTENUATION, .1);
		glPopMatrix();
	}
	glPushMatrix();
	glTranslatef(pos[0], pos[1], pos[2]-0.3);
	GLfloat amb[4] = {0.05, 0.05, 0.0, 1.0};
	GLfloat dif[4] = { 0.5, 0.5, 0.4, 1.0 };
	GLfloat spe[4] = { 0.7, 0.7, 0.04, 1.0 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, dif);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spe);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 75);
	glutSolidCone(0.2, 0.3, 20, 20);
	glPopMatrix();
	/*std::cout << "enum: " << num << " x: " << position.getX() << " y: " << position.getY() << " z: " << position.getZ() << std::endl;*/
}