#include "Camera.h"

GLdouble Camera::getNear() const
{
	return near;
}

GLdouble Camera::getFar() const
{
	return far;
}

GLboolean Camera::isActive() const {
	return active;
}

Vector3 Camera::getUp() const
{
	return up;
}

Vector3 Camera::getCenter() const
{
	return center;
}

Vector3 Camera::getAt() const
{
	return at;
}

void Camera::setNear(GLdouble n)
{
	near = n;
}

void Camera::setFar(GLdouble f)
{
	far = f;
}

void Camera::flipActive() {
	active = !active;
}

void Camera::setUp(GLdouble x, GLdouble y, GLdouble z)
{
	up.set(x, y, z);
}

void Camera::setCenter(GLdouble x, GLdouble y, GLdouble z)
{
	center.set(x, y, z);
}

void Camera::setAt(GLdouble x, GLdouble y, GLdouble z)
{
	at.set(x, y, z);
}
