#include "Vector3.h"
#include "Solids.h"
#include <cmath>

#define PI 3.1415926535897

void DrawTorus(float majorRadius, float minorRadius, int numMajor, int numMinor)
{
    Vector3 *vNormal = new Vector3();
    double majorStep = 2.0f * PI / numMajor;
    double minorStep = 2.0f * PI / numMinor;
    int i, j;

    for (i = 0; i < numMajor; i++)
    {
        double a0 = i * majorStep;
        double a1 = a0 + majorStep;
        float x0 = (float)cos(a0);
        float y0 = (float)sin(a0);
        float x1 = (float)cos(a1);
        float y1 = (float)sin(a1);

        glBegin(GL_TRIANGLE_STRIP);
        for (j = 0; j <= numMinor; j++)
        {
            double b = j * minorStep;
            float c = (float)cos(b);
            float r = minorRadius * c + majorRadius;
            float z = minorRadius * (float)sin(b);

            //First point
            // glTexCoord2f((float)i / (float)(numMajor), (float)(j) / (float)(numMinor));
            vNormal->setX(x0 * c);
            vNormal->setY(y0 * c);
            vNormal->setZ(z / minorRadius);
            vNormal->normalize();
            glNormal3f(vNormal->getX(), vNormal->getY(), vNormal->getZ());
            glVertex3f(x0 * r, y0 * r, z);

            //glTexCoord2f((float)(i + 1) / (float)(numMajor), (float)(j) / (float)(numMinor));
            vNormal->setX(x1 * c);
            vNormal->setY(y1 * c);
            vNormal->setZ(z / minorRadius);
            vNormal->normalize();
            glNormal3f(vNormal->getX(), vNormal->getY(), vNormal->getZ());
            glVertex3f(x1 * r, y1 * r, z);
        }
        glEnd();
    }

}
