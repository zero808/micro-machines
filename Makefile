appname := micro

#delete the include/GL directory first if you have glut installed
IDIR=include
CXX := g++
CXXFLAGS := -std=c++11 -I$(IDIR) -O0 -ggdb
LDFLAGS= -lGL -lGLU -lglut

srcfiles := $(shell find src -name "*.cpp")
objects  := $(patsubst %.cpp, %.o, $(srcfiles))

all: $(appname)

$(appname): $(objects)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $(appname) $(objects) $(LDLIBS)

clean:
	rm -f $(objects)
