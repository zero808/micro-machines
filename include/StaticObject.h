#ifndef __STATICOBJECT_H__
#define __STATICOBJECT_H__

#include "GameObject.h"
#include <GL/glut.h>

class StaticObject : public GameObject {

    public:
        StaticObject() {}
        virtual ~StaticObject() {}
};

#endif

