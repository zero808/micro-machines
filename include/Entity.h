#ifndef __ENTITY_H__
#define __ENTITY_H__

#include <vector>
/* #include <GLUT.h> */
#include "Vector3.h"
#include <GL/glut.h>

class Entity {
    protected:
        /* std::vector<Vector3*> position; */
        //Vector3 *position;
		Vector3 position;

    public:
        Entity();
        virtual ~Entity();

        Vector3 getPosition() const;
        Vector3 setPosition(GLdouble x, GLdouble y, GLdouble z);
        Vector3 setPosition(const Vector3 &vec);
        /* Vector3* setPosition(const Vector3* vec); */
};
#endif
