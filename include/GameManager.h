#ifndef __GAMEMANAGER_H__
#define __GAMEMANAGER_H__

#include <vector>
#include <array>
#include "DynamicObject.h"
#include "PerspectiveCamera.h"
#include "OrthogonalCamera.h"
#include "LightSource.h"
#include "PointLight.h"
#include "Table.h"
#include "Road.h"
#include "Car.h"
#include <GL/glut.h>

//constantes da camara ortogonal (1)
#define XMIN -10
#define XMAX 10
#define YMIN -10
#define YMAX 10
#define ZMIN -2
#define ZMAX 2
#define BUTTER_AMOUNT 5
#define ORANGE_AMOUNT 3
#define CHEERIO_AMOUNT 12
#define CANDLES_AMOUNT 6

class GameManager : public Entity {

    private:
        std::vector<DynamicObject*> game_objects;
        std::array<PointLight, CANDLES_AMOUNT> candles;
        bool wired;
		Car *car;
		Table *table;
		Road *road;
		OrthogonalCamera *ocam;
		PerspectiveCamera *fcam;
		PerspectiveCamera *carcam;
		LightSource *light;

    public:
        GameManager();
        ~GameManager();

        void display();
        void reshape(GLsizei w, GLsizei h);
        void keyPressed(unsigned char key, int x, int y);
        void keyReleased(unsigned char key, int x, int y);
		void specialKeyPressed(int key, int x, int y);
        void onTimer();
        void idle();
        void update();
        void init();
        void flipWired();
        bool getWired();
        PerspectiveCamera* getCarcam();
        void collision();
		void flipCandles();

};
extern GameManager gm;

void display();
void reshape(GLsizei w, GLsizei h);
void keyPressed(unsigned char key, int x, int y);
void specialKeyPressed(int key, int x, int y);
void keyUp(unsigned char key, int x, int y);
void timer(int value);
void update(int i);

#endif
