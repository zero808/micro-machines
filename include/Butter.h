#ifndef __BUTTER_H__
#define __BUTTER_H__

#include "Obstacle.h"
#include <GL/glut.h>

class Butter: public Obstacle {

    public:
        Butter() {}
		Butter(GLdouble x, GLdouble y, GLdouble z);
        virtual ~Butter() {}

        void draw();
		void update(GLdouble delta_t);

};

#endif


