#ifndef __DYNAMICOBJECT_H__
#define __DYNAMICOBJECT_H__

#include "GameObject.h"
#include "Vector3.h"
#include <GL/glut.h>

#define PI 3.1415926535897

class DynamicObject : public GameObject {

    protected:
        //std::vector<Vector3*>
		Vector3 speed;
		GLdouble rotation;
		GLdouble collisionRadius;
		GLdouble alive;

    public:
        DynamicObject();
        virtual ~DynamicObject() {}

		GLdouble getAlive();
		void setAlive(GLdouble tempo);
		GLdouble getCollisionRadius();
		void setCollisionRadius(GLdouble radius);
		GLdouble getRotation();
		void setRotation(GLdouble angle);
        virtual void update(GLdouble delta_t);
        void setSpeed(const Vector3& v);
        void setSpeed(GLdouble x, GLdouble y, GLdouble z);
        Vector3 getSpeed() const;

};

#endif

