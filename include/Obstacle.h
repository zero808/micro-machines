#ifndef __OBSTACLE_H__
#define __OBSTACLE_H__

#include "DynamicObject.h"
#include <GL/glut.h>

class Obstacle : public DynamicObject {

	public:
		Obstacle() {}
		Obstacle(GLdouble x, GLdouble y, GLdouble z);
		virtual ~Obstacle() {}

};

#endif

