#ifndef __SOLIDS_H__
#define __SOLIDS_H__

#include "GL/glut.h"

void DrawTorus(float majorRadius, float minorRadius, int numMajor, int numMinor);

#endif