#ifndef __GAMEOBJECT_H__
#define __GAMEOBJECT_H__

#include "Entity.h"
#include <GL/glut.h>

class GameObject : public Entity {

    public:
        GameObject() {}
        virtual ~GameObject() {}

        virtual void draw() = 0;
        virtual void update(GLdouble delta);
		void defineMaterial(GLfloat a_r, GLfloat a_g, GLfloat a_b, GLfloat a_w,
			GLfloat d_r, GLfloat d_g, GLfloat d_b, GLfloat d_w,
			GLfloat s_r, GLfloat s_g, GLfloat s_b, GLfloat s_w,
			//GLfloat e_r, GLfloat e_g, GLfloat e_b, GLfloat e_w,
			GLfloat Si);
};

#endif
