#ifndef __LIGHTSOURCE_H__
#define __LIGHTSOURCE_H__

#include <array>
#include "Vector3.h"
#include <GL/glut.h>

#define MAIN_LIGHT_AMBIENTR 0.3
#define MAIN_LIGHT_AMBIENTG 0.3
#define MAIN_LIGHT_AMBIENTB 0.3
#define MAIN_LIGHT_AMBIENTA 1
#define MAIN_LIGHT_DIFFUSER 0.7
#define MAIN_LIGHT_DIFFUSEG 0.7
#define MAIN_LIGHT_DIFFUSEB 0.7
#define MAIN_LIGHT_DIFFUSEA 1
#define MAIN_LIGHT_SPECULARR 0.5
#define MAIN_LIGHT_SPECULARG 0.5
#define MAIN_LIGHT_SPECULARB 0.5
#define MAIN_LIGHT_SPECULARA 1

class LightSource {

    protected:
        std::array<GLfloat, 4> ambient;
        std::array<GLfloat, 4> diffuse;
        std::array<GLfloat, 4> specular;
        GLdouble cut_off;
        GLdouble exponent;
        GLenum num;
        GLboolean state;
		Vector3 direction;
		Vector3 position;

    public:
        LightSource() : state(false) {}
        LightSource(GLenum _num, GLboolean st) : num(_num), state(st) {}
		virtual ~LightSource() {}

		std::array<GLfloat, 4> getAmbient() const;
		void setAmbient(GLfloat r, GLfloat g, GLfloat b, GLfloat a);
		std::array<GLfloat, 4> getDiffuse() const;
		void setDiffuse(GLfloat r, GLfloat g, GLfloat b, GLfloat a);
		std::array<GLfloat, 4> getSpecular() const;
		void setSpecular(GLfloat r, GLfloat g, GLfloat b, GLfloat a);
		GLdouble getCutOff() const;
		void setCutOff(GLdouble);
		GLdouble getExponent() const;
		void setExponent(GLdouble);
        GLboolean getState() const;
        void flipState();
		void setNum(GLenum);
        GLenum getNum() const;

		Vector3 getDirection() const;
		void setDirection(GLdouble x, GLdouble y, GLdouble);
		Vector3 getPosition() const;
		void setPosition(GLdouble x, GLdouble y, GLdouble);
		virtual void draw();

};

#endif
