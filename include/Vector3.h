#ifndef __VECTOR_H__
#define __VECTOR_H__

#include <GL/glut.h>

class Vector3 {

    protected:
        GLdouble x;
        GLdouble y;
        GLdouble z;

    public:
        Vector3() : x(0), y(0), z(0) {}
        Vector3(GLdouble _x, GLdouble _y, GLdouble _z) : x(_x), y(_y), z(_z) {}
        ~Vector3() {}

        GLdouble getX() { return x; }
        GLdouble getY() { return y; }
        GLdouble getZ() { return z; }

        void setX(GLdouble _x) { x = _x; }
        void setY(GLdouble _y) { x = _y; }
        void setZ(GLdouble _z) { x = _z; }

        void set(GLdouble _x, GLdouble _y, GLdouble _z) { x = _x; y = _y; z = _z; }

        Vector3 operator=(const Vector3& other);
        Vector3 operator+(const Vector3& other);
        Vector3 operator*(GLdouble num);
        Vector3 operator-(const Vector3& other);
        bool operator==(const Vector3& other);

		void normalize();

};
#endif
