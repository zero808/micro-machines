#ifndef __ORANGE_H__
#define __ORANGE_H__

#include "Obstacle.h"
#include <GL/glut.h>

class Orange : public Obstacle {

	private:
		GLdouble roll;

    public:
		Orange() {}
		Orange(GLdouble x, GLdouble y, GLdouble z);
        virtual ~Orange() {}

		GLdouble getRoll();
		void setRoll(GLdouble rolamento);
        void draw();
		void update(GLdouble delta_t);
};

#endif


