#ifndef __ROAD_H__
#define __ROAD_H__

#include "StaticObject.h"
#include <GL/glut.h>

class Road : public StaticObject {

    public:
        Road() {}
        ~Road() {}

        void draw();
};

#endif
