#ifndef __CAR_H__
#define __CAR_H__

#define CAR_INITIAL_POSITION_X 0.0f
#define CAR_INITIAL_POSITION_Y 0.0f
#define CAR_INITIAL_POSITION_Z 0.0f

#include "DynamicObject.h"
#include <GL/glut.h>

class Car : public DynamicObject {

    private:
		bool direction;

    public:
        Car();
        virtual ~Car() {}

		void flipDirection();
		bool getDirection();
		void drawwheel(GLdouble x, GLdouble y, GLdouble z, bool wired);
		void drawCube(GLdouble x, GLdouble y, GLdouble z, bool wired, GLdouble size);
		void draw();
		void update(GLdouble delta_t);

};

#endif
