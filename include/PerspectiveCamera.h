#ifndef __PERSPECTIVECAMERA_H__
#define __PERSPECTIVECAMERA_H___

#include "Camera.h"
#include "Car.h"
#include <GL/glut.h>

#define FIXED_CAMERA_ASPECT 1
#define FIXED_CAMERA_FOVY 90
#define FIXED_CAMERA_NEAR 0.001f
#define FIXED_CAMERA_FAR 100.0f
#define FIXED_CAMERA_EYEX 0.0f
#define FIXED_CAMERA_EYEY -8.0f
#define FIXED_CAMERA_EYEZ 11.5f
#define FIXED_CAMERA_CENTERX 0
#define FIXED_CAMERA_CENTERY 0
#define FIXED_CAMERA_CENTERZ 0
#define FIXED_CAMERA_UPX 0
#define FIXED_CAMERA_UPY 1
#define FIXED_CAMERA_UPZ 0

#define CAR_CAMERA_ASPECT 1
#define CAR_CAMERA_FOVY 90
#define CAR_CAMERA_NEAR 0.01f
#define CAR_CAMERA_FAR 100.0f
// same as the car so we never actually use this
#define CAR_CAMERA_EYEX -1.0f //relativamente ao carro
#define CAR_CAMERA_EYEY -1.0f //relativamente ao carro
#define CAR_CAMERA_EYEZ 0.5f //relativamente ao carro
#define CAR_CAMERA_CENTERX 0
#define CAR_CAMERA_CENTERY 0
#define CAR_CAMERA_CENTERZ 0
#define CAR_CAMERA_UPX 0
#define CAR_CAMERA_UPY 0
#define CAR_CAMERA_UPZ 1

class PerspectiveCamera : public Camera {

    protected:
        GLdouble fovy;
        GLdouble aspect;

    public:
        PerspectiveCamera(GLdouble _fovy, GLdouble _aspect, GLdouble _near, GLdouble _far)
            : Camera(_near, _far, false), fovy(_fovy), aspect(_aspect) {}
        ~PerspectiveCamera() {}

		GLdouble getFovy() const;
		GLdouble getAspect() const;
		void setFovy(GLdouble);
		void setAspect(GLdouble);
        void update(Car*);
        void computeProjectionMatrix();
        void computeVisualizationMatrix();
};

#endif


