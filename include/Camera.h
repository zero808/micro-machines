#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "Entity.h"
#include "Vector3.h"
#include "Car.h"
#include <GL/glut.h>

class Camera : public Entity {

    protected:
        GLdouble near;
        GLdouble far;
		GLboolean active;
		Vector3 up;
		Vector3 center;
		Vector3 at;

    public:
		Camera(GLdouble n, GLdouble f, GLboolean a) : near(n), far(f), active(a) {}
        virtual ~Camera() {}

		GLdouble getNear() const;
		GLdouble getFar() const;
		GLboolean isActive() const;
		Vector3 getUp() const;
		Vector3 getCenter() const;
		Vector3 getAt() const;
		void setNear(GLdouble);
		void setFar(GLdouble);
		void flipActive();
		void setUp(GLdouble x, GLdouble y, GLdouble z);
		void setCenter(GLdouble x, GLdouble y, GLdouble z);
		void setAt(GLdouble x, GLdouble y, GLdouble z);

        virtual void update(Car*) = 0;
        virtual void computeProjectionMatrix() = 0;
        virtual void computeVisualizationMatrix() = 0;
};

#endif
