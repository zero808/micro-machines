#ifndef __ORTHOGONALCAMERA_H__
#define __ORTHOGONALCAMERA_H__ 

#include "Camera.h"
#include <GL/glut.h>

#define ORTHOGONAL_CAMERA_LEFT -10.0f
#define ORTHOGONAL_CAMERA_RIGHT 10.0f
#define ORTHOGONAL_CAMERA_TOP 10.0f
#define ORTHOGONAL_CAMERA_BOTTOM -10.0f
#define ORTHOGONAL_CAMERA_NEAR -10.0f
#define ORTHOGONAL_CAMERA_FAR 10.0f

class OrthogonalCamera : public Camera {

    protected:
        GLdouble left;
        GLdouble right;
        GLdouble bottom;
        GLdouble top;

    public:
		OrthogonalCamera() : Camera(ORTHOGONAL_CAMERA_NEAR, ORTHOGONAL_CAMERA_FAR, true), left(ORTHOGONAL_CAMERA_LEFT),
			right(ORTHOGONAL_CAMERA_RIGHT), bottom(ORTHOGONAL_CAMERA_BOTTOM), top(ORTHOGONAL_CAMERA_TOP) {}
        OrthogonalCamera(GLdouble _left, GLdouble _right, GLdouble _bottom, GLdouble _top,
                        GLdouble _near, GLdouble _far)
            : Camera(_near, _far, true), left(_left), right(_right), bottom(_bottom), top(_top) {}
        ~OrthogonalCamera() {}


		GLdouble getLeft() const;
		GLdouble getRight() const;
		GLdouble getBottom() const;
		GLdouble getTop() const;
		void setLeft(GLdouble);
		void setRight(GLdouble);
		void setBottom(GLdouble);
		void setTop(GLdouble);
        void update(Car*);
        void computeProjectionMatrix();
        void computeVisualizationMatrix();
		
};

#endif

