#include "LightSource.h"

class PointLight : public LightSource {
	public:
		PointLight();
		PointLight(GLenum _num, GLboolean st);
		~PointLight();
		void draw();
};
