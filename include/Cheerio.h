#ifndef __CHEERIO_H__
#define __CHEERIO_H__

#include "Obstacle.h"
#include <GL/glut.h>

class Cheerio : public Obstacle {

private:
	GLdouble roll;
	GLdouble alive;

public:
	Cheerio() {}
	Cheerio(GLdouble x, GLdouble y, GLdouble z);
	virtual ~Cheerio() {}

	void draw();
	void update(GLdouble delta_t);
};

#endif